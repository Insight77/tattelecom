﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattelcom_Project
{
    class Clients : Form
    {
        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
        private string selectCommand = "SELECT client_id, fullname, passport_number, address, phone FROM Clients"; 

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        DataTable dataTable = new DataTable();

        private bool openNewTask = false;
        public string clientFullName;
        public string clientPassNumber;
        public string clientAddress;
        public string clientPhone;

        private DataGridView dataGridView1;
        private Button search_button1;
        private Button button1;
        private TextBox search_textBox1;

        public Clients(bool openNewTask)
        {
            InitializeComponent();
            loadDataGrid(selectCommand);
            dataGridViewConfiguration();
            this.openNewTask = openNewTask;
            button1.Enabled = false;
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.search_button1 = new System.Windows.Forms.Button();
            this.search_textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 39);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(595, 366);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // search_button1
            // 
            this.search_button1.Location = new System.Drawing.Point(532, 13);
            this.search_button1.Name = "search_button1";
            this.search_button1.Size = new System.Drawing.Size(75, 23);
            this.search_button1.TabIndex = 1;
            this.search_button1.Text = "Найти";
            this.search_button1.UseVisualStyleBackColor = true;
            this.search_button1.Click += new System.EventHandler(this.search_button1_Click);
            // 
            // search_textBox1
            // 
            this.search_textBox1.Location = new System.Drawing.Point(276, 15);
            this.search_textBox1.Name = "search_textBox1";
            this.search_textBox1.Size = new System.Drawing.Size(250, 20);
            this.search_textBox1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Image = global::Tattelcom_Project.Properties.Resources.backImage;
            this.button1.Location = new System.Drawing.Point(250, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 20);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Clients
            // 
            this.ClientSize = new System.Drawing.Size(619, 413);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.search_textBox1);
            this.Controls.Add(this.search_button1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clients";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Клиенты";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Clients_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void loadDataGrid(string SQLcommand)
        {
            dataTable.Clear();
            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
        }

        private void dataGridViewConfiguration()
        {
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].Width = 200; // fullname
            dataGridView1.Columns[2].Width = 100; // passport
            dataGridView1.Columns[3].Width = 200; // addres
            dataGridView1.Columns[4].Width = 90; // Client phone

            dataGridView1.RowHeadersVisible = false;

            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);


            dataGridView1.ReadOnly = true;
        }

        private void search_button1_Click(object sender, EventArgs e)
        {
            //поисковая строка 
            string word = search_textBox1.Text;

            //строка запроса с фильтром 
            string queryString = "SELECT client_id, fullname, passport_number, address, phone FROM Clients " +
                                       " WHERE (fullname LIKE '%" + word + "%'" +
                                       " OR passport_number LIKE '%" + word + "%'" +
                                       " OR address LIKE '%" + word + "%'" +
                                       " OR phone LIKE '%" + word + "%')";

            //вызов метода для заполнения грида с данными найденными по  фильтру 
            loadDataGrid(queryString);

            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadDataGrid(selectCommand);
            button1.Enabled = false;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (openNewTask)
            {
                int selectedRow = dataGridView1.CurrentRow.Index; // Выделенная строка
                clientFullName = dataGridView1[1, selectedRow].Value.ToString();
                clientPassNumber = dataGridView1[2, selectedRow].Value.ToString();
                clientAddress = dataGridView1[3, selectedRow].Value.ToString();
                clientPhone = dataGridView1[4, selectedRow].Value.ToString();
                this.Close();
            }
        }

        private void Clients_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataBase.Close();
        }
    }
}
