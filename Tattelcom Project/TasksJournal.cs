﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattelcom_Project
{
    class TasksJournal : Form
    {
        //_______________________________Запрещение перемещение формы__________________________________//

        const int SC_CLOSE = 0xF010;
        const int MF_BYCOMMAND = 0;
        const int WM_NCLBUTTONDOWN = 0x00A1;
        const int WM_NCHITTEST = 0x0084;
        private DataGridView dataGridView1;
        private Button search_button1;
        private TextBox search_textBox1;
        private Button button1;
        const int HTCAPTION = 2;
        [DllImport("User32.dll")]
        static extern int SendMessage(IntPtr hWnd,
        int Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("User32.dll")]
        static extern bool RemoveMenu(IntPtr hMenu, int uPosition, int uFlags);

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_NCLBUTTONDOWN)
            {
                int result = SendMessage(m.HWnd, WM_NCHITTEST,
                IntPtr.Zero, m.LParam);
                if (result == HTCAPTION)
                    return;
            }
            base.WndProc(ref m);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            IntPtr hMenu = GetSystemMenu(Handle, false);
            RemoveMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
        }

        //___________________________________________________________________________________________//

        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
        private string selectCommand = "SELECT task_id, Task_types.type_name, Users.fullname, Clients.fullname, Clients.phone,  date_begin, date_end, Tasks.user_id, Tasks.type_id, Tasks.client_id, date_begin_hour" +
                                       " FROM Tasks, Users, Task_types, Clients" +
                                       " WHERE Tasks.user_id = Users.id AND Tasks.type_id = Task_types.task_type_id AND Tasks.client_id = Clients.client_id";

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        DataTable dataTable = new DataTable();


        public TasksJournal()
        {
            InitializeComponent();
            loadDataGrid(selectCommand);
            dataGridViewConfiguration();
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.search_button1 = new System.Windows.Forms.Button();
            this.search_textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 39);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(952, 418);
            this.dataGridView1.TabIndex = 0;
            // 
            // search_button1
            // 
            this.search_button1.Location = new System.Drawing.Point(889, 10);
            this.search_button1.Name = "search_button1";
            this.search_button1.Size = new System.Drawing.Size(75, 23);
            this.search_button1.TabIndex = 1;
            this.search_button1.Text = "Найти";
            this.search_button1.UseVisualStyleBackColor = true;
            this.search_button1.Click += new System.EventHandler(this.search_button1_Click);
            // 
            // search_textBox1
            // 
            this.search_textBox1.Location = new System.Drawing.Point(566, 12);
            this.search_textBox1.Name = "search_textBox1";
            this.search_textBox1.Size = new System.Drawing.Size(316, 20);
            this.search_textBox1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Image = global::Tattelcom_Project.Properties.Resources.backImage;
            this.button1.Location = new System.Drawing.Point(540, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 20);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TasksJournal
            // 
            this.ClientSize = new System.Drawing.Size(976, 462);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.search_textBox1);
            this.Controls.Add(this.search_button1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TasksJournal";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Журнал заявок";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TasksJournal_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void loadDataGrid(string SQLcommand)
        {
            dataTable.Clear();
            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
        }

        private void dataGridViewConfiguration()
        {
            dataGridView1.Columns[0].Visible = false; // id
            dataGridView1.Columns[1].Width = 260; // вид услуги
            dataGridView1.Columns[2].Width = 200; // Client FIO
            dataGridView1.Columns[3].Width = 200; // Client phone
            dataGridView1.Columns[4].Width = 90; // User FIO
            dataGridView1.Columns[5].Width = 100; // дата начала   
            dataGridView1.Columns[6].Width = 100; // дата завершения
            dataGridView1.Columns[7].Visible = false; // Tasks.user_id
            dataGridView1.Columns[8].Visible = false; // Tasks.type_id
            dataGridView1.Columns[9].Visible = false; // Tasks.clients_id
            dataGridView1.Columns[10].Visible = false; // Date_begin_hour

            dataGridView1.RowHeadersVisible = false;

            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);


            dataGridView1.ReadOnly = true;
        }

        private void search_button1_Click(object sender, EventArgs e)
        {
            //поисковая строка 
            string word = search_textBox1.Text;

            //строка запроса с фильтром 
            string queryString = "SELECT task_id, Task_types.type_name, Users.fullname, Clients.fullname, Clients.phone, date_begin, date_end, Tasks.user_id, Tasks.type_id, Tasks.client_id, date_begin_hour" +
                                       " FROM Tasks, Users, Task_types, Clients" +
                                       " WHERE Tasks.user_id = Users.id AND Tasks.type_id = Task_types.task_type_id AND Tasks.client_id = Clients.client_id" +
                                       " AND (Users.fullname LIKE '%" + word + "%'" + 
                                       " OR Clients.fullname LIKE '%" + word + "%'" +
                                       " OR Clients.phone LIKE '%" + word + "%'" +
                                       " OR Task_types.type_name LIKE '%" + word + "%'" +
                                       " OR date_begin LIKE '%" + word + "%'" +
                                       " OR date_end LIKE '%" + word + "%')";

            //вызов метода для заполнения грида с данными найденными по  фильтру 
            loadDataGrid(queryString);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadDataGrid(selectCommand);
        }

        private void TasksJournal_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataBase.Close();
        }
    }
}
