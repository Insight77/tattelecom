﻿namespace Tattelcom_Project
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.администраторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сотрудникToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заявкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.журналЗаявокToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.гистограммаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.списокСотрудниковToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.сотрудникToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяЗаявкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.клиентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.моиЗаявкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.учетнаяКарточкаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инструкцияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.personName_label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.администраторToolStripMenuItem,
            this.сотрудникToolStripMenuItem1,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1080, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // администраторToolStripMenuItem
            // 
            this.администраторToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сотрудникToolStripMenuItem,
            this.заявкиToolStripMenuItem,
            this.журналЗаявокToolStripMenuItem1,
            this.гистограммаToolStripMenuItem,
            this.списокСотрудниковToolStripMenuItem1});
            this.администраторToolStripMenuItem.Enabled = false;
            this.администраторToolStripMenuItem.Name = "администраторToolStripMenuItem";
            this.администраторToolStripMenuItem.Size = new System.Drawing.Size(106, 20);
            this.администраторToolStripMenuItem.Text = "Администратор";
            this.администраторToolStripMenuItem.MouseEnter += new System.EventHandler(this.администраторToolStripMenuItem_MouseEnter);
            this.администраторToolStripMenuItem.MouseLeave += new System.EventHandler(this.администраторToolStripMenuItem_MouseLeave);
            // 
            // сотрудникToolStripMenuItem
            // 
            this.сотрудникToolStripMenuItem.Name = "сотрудникToolStripMenuItem";
            this.сотрудникToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.сотрудникToolStripMenuItem.Text = "Сотрудники";
            this.сотрудникToolStripMenuItem.Click += new System.EventHandler(this.сотрудникToolStripMenuItem_Click);
            this.сотрудникToolStripMenuItem.MouseEnter += new System.EventHandler(this.сотрудникToolStripMenuItem_MouseEnter);
            this.сотрудникToolStripMenuItem.MouseLeave += new System.EventHandler(this.сотрудникToolStripMenuItem_MouseLeave);
            // 
            // заявкиToolStripMenuItem
            // 
            this.заявкиToolStripMenuItem.Name = "заявкиToolStripMenuItem";
            this.заявкиToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.заявкиToolStripMenuItem.Text = "Виды услуг";
            this.заявкиToolStripMenuItem.Click += new System.EventHandler(this.заявкиToolStripMenuItem_Click);
            this.заявкиToolStripMenuItem.MouseEnter += new System.EventHandler(this.заявкиToolStripMenuItem_MouseEnter);
            this.заявкиToolStripMenuItem.MouseLeave += new System.EventHandler(this.заявкиToolStripMenuItem_MouseLeave);
            // 
            // журналЗаявокToolStripMenuItem1
            // 
            this.журналЗаявокToolStripMenuItem1.Name = "журналЗаявокToolStripMenuItem1";
            this.журналЗаявокToolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
            this.журналЗаявокToolStripMenuItem1.Text = "Журнал заявок";
            this.журналЗаявокToolStripMenuItem1.Click += new System.EventHandler(this.журналЗаявокToolStripMenuItem1_Click);
            this.журналЗаявокToolStripMenuItem1.MouseEnter += new System.EventHandler(this.журналЗаявокToolStripMenuItem1_MouseEnter);
            this.журналЗаявокToolStripMenuItem1.MouseLeave += new System.EventHandler(this.журналЗаявокToolStripMenuItem1_MouseLeave);
            // 
            // гистограммаToolStripMenuItem
            // 
            this.гистограммаToolStripMenuItem.Name = "гистограммаToolStripMenuItem";
            this.гистограммаToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.гистограммаToolStripMenuItem.Text = "Гистограмма";
            this.гистограммаToolStripMenuItem.Click += new System.EventHandler(this.гистограммаToolStripMenuItem_Click);
            // 
            // списокСотрудниковToolStripMenuItem1
            // 
            this.списокСотрудниковToolStripMenuItem1.Name = "списокСотрудниковToolStripMenuItem1";
            this.списокСотрудниковToolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
            this.списокСотрудниковToolStripMenuItem1.Text = "Отчет по сотрудникам";
            this.списокСотрудниковToolStripMenuItem1.Click += new System.EventHandler(this.списокСотрудниковToolStripMenuItem1_Click);
            this.списокСотрудниковToolStripMenuItem1.MouseEnter += new System.EventHandler(this.списокСотрудниковToolStripMenuItem1_MouseEnter);
            this.списокСотрудниковToolStripMenuItem1.MouseLeave += new System.EventHandler(this.списокСотрудниковToolStripMenuItem1_MouseLeave);
            // 
            // сотрудникToolStripMenuItem1
            // 
            this.сотрудникToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяЗаявкаToolStripMenuItem,
            this.клиентыToolStripMenuItem,
            this.моиЗаявкиToolStripMenuItem,
            this.учетнаяКарточкаToolStripMenuItem1});
            this.сотрудникToolStripMenuItem1.Name = "сотрудникToolStripMenuItem1";
            this.сотрудникToolStripMenuItem1.Size = new System.Drawing.Size(78, 20);
            this.сотрудникToolStripMenuItem1.Text = "Сотрудник";
            this.сотрудникToolStripMenuItem1.MouseEnter += new System.EventHandler(this.сотрудникToolStripMenuItem1_MouseEnter);
            this.сотрудникToolStripMenuItem1.MouseLeave += new System.EventHandler(this.сотрудникToolStripMenuItem1_MouseLeave);
            // 
            // новаяЗаявкаToolStripMenuItem
            // 
            this.новаяЗаявкаToolStripMenuItem.Name = "новаяЗаявкаToolStripMenuItem";
            this.новаяЗаявкаToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.новаяЗаявкаToolStripMenuItem.Text = "Новая заявка";
            this.новаяЗаявкаToolStripMenuItem.Click += new System.EventHandler(this.новаяЗаявкаToolStripMenuItem_Click);
            this.новаяЗаявкаToolStripMenuItem.MouseEnter += new System.EventHandler(this.новаяЗаявкаToolStripMenuItem_MouseEnter);
            this.новаяЗаявкаToolStripMenuItem.MouseLeave += new System.EventHandler(this.новаяЗаявкаToolStripMenuItem_MouseLeave);
            // 
            // клиентыToolStripMenuItem
            // 
            this.клиентыToolStripMenuItem.Name = "клиентыToolStripMenuItem";
            this.клиентыToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.клиентыToolStripMenuItem.Text = "Клиенты";
            this.клиентыToolStripMenuItem.Click += new System.EventHandler(this.клиентыToolStripMenuItem_Click);
            this.клиентыToolStripMenuItem.MouseEnter += new System.EventHandler(this.клиентыToolStripMenuItem_MouseEnter);
            this.клиентыToolStripMenuItem.MouseLeave += new System.EventHandler(this.клиентыToolStripMenuItem_MouseLeave);
            // 
            // моиЗаявкиToolStripMenuItem
            // 
            this.моиЗаявкиToolStripMenuItem.Name = "моиЗаявкиToolStripMenuItem";
            this.моиЗаявкиToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.моиЗаявкиToolStripMenuItem.Text = "Мои заявки";
            this.моиЗаявкиToolStripMenuItem.Click += new System.EventHandler(this.журналЗаявокToolStripMenuItem_Click);
            this.моиЗаявкиToolStripMenuItem.MouseEnter += new System.EventHandler(this.журналЗаявокToolStripMenuItem_MouseEnter);
            this.моиЗаявкиToolStripMenuItem.MouseLeave += new System.EventHandler(this.журналЗаявокToolStripMenuItem_MouseLeave);
            // 
            // учетнаяКарточкаToolStripMenuItem1
            // 
            this.учетнаяКарточкаToolStripMenuItem1.Enabled = false;
            this.учетнаяКарточкаToolStripMenuItem1.Name = "учетнаяКарточкаToolStripMenuItem1";
            this.учетнаяКарточкаToolStripMenuItem1.Size = new System.Drawing.Size(171, 22);
            this.учетнаяКарточкаToolStripMenuItem1.Text = "Учетная карточка";
            this.учетнаяКарточкаToolStripMenuItem1.MouseEnter += new System.EventHandler(this.учетнаяКарточкаToolStripMenuItem1_MouseEnter);
            this.учетнаяКарточкаToolStripMenuItem1.MouseLeave += new System.EventHandler(this.учетнаяКарточкаToolStripMenuItem1_MouseLeave);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.инструкцияToolStripMenuItem,
            this.оПрограммеToolStripMenuItem1});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
            this.справкаToolStripMenuItem.MouseEnter += new System.EventHandler(this.справкаToolStripMenuItem_MouseEnter);
            this.справкаToolStripMenuItem.MouseLeave += new System.EventHandler(this.справкаToolStripMenuItem_MouseLeave);
            // 
            // инструкцияToolStripMenuItem
            // 
            this.инструкцияToolStripMenuItem.Enabled = false;
            this.инструкцияToolStripMenuItem.Name = "инструкцияToolStripMenuItem";
            this.инструкцияToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.инструкцияToolStripMenuItem.Text = "Инструкция";
            this.инструкцияToolStripMenuItem.MouseEnter += new System.EventHandler(this.инструкцияToolStripMenuItem_MouseEnter);
            this.инструкцияToolStripMenuItem.MouseLeave += new System.EventHandler(this.инструкцияToolStripMenuItem_MouseLeave);
            // 
            // оПрограммеToolStripMenuItem1
            // 
            this.оПрограммеToolStripMenuItem1.Name = "оПрограммеToolStripMenuItem1";
            this.оПрограммеToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.оПрограммеToolStripMenuItem1.Text = "О программе";
            this.оПрограммеToolStripMenuItem1.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem1_Click);
            this.оПрограммеToolStripMenuItem1.MouseEnter += new System.EventHandler(this.оПрограммеToolStripMenuItem1_MouseEnter);
            this.оПрограммеToolStripMenuItem1.MouseLeave += new System.EventHandler(this.оПрограммеToolStripMenuItem1_MouseLeave);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 536);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1080, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolStripStatusLabel1_MouseDown);
            this.toolStripStatusLabel1.MouseLeave += new System.EventHandler(this.toolStripStatusLabel1_MouseLeave);
            // 
            // personName_label1
            // 
            this.personName_label1.AutoSize = true;
            this.personName_label1.BackColor = System.Drawing.SystemColors.Window;
            this.personName_label1.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.personName_label1.Location = new System.Drawing.Point(728, 0);
            this.personName_label1.Name = "personName_label1";
            this.personName_label1.Size = new System.Drawing.Size(44, 18);
            this.personName_label1.TabIndex = 3;
            this.personName_label1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1080, 558);
            this.Controls.Add(this.personName_label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tattelecom";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem администраторToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сотрудникToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сотрудникToolStripMenuItem1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem инструкцияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem списокСотрудниковToolStripMenuItem1;
        private System.Windows.Forms.Label personName_label1;
        private System.Windows.Forms.ToolStripMenuItem новаяЗаявкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem клиентыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem учетнаяКарточкаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem заявкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem моиЗаявкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem журналЗаявокToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem гистограммаToolStripMenuItem;
    }
}

