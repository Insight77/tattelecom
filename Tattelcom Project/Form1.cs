﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattelcom_Project
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void toolStripStatusLabel1_MouseDown(object sender, MouseEventArgs e)
        {
            toolStripStatusLabel1.Text = toolStripStatusLabel1.Text;
        }

        private void toolStripStatusLabel1_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void сотрудникToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = сотрудникToolStripMenuItem.Text;
        }

        private void сотрудникToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }


        private void справкаToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = справкаToolStripMenuItem.Text;
        }

        private void справкаToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try {
                AuthClass AC = new AuthClass();
                // Установить родительское окно для дочернего
                AC.Owner = this;
                // Вывести на экран дочерее окно
                AC.ShowDialog();

                this.personName_label1.Text = AC.personName;
                personName_label1.Location = new Point(800 - AC.personName.Length, 4);
                if (AC.admin == 1) администраторToolStripMenuItem.Enabled = true;
            }
            catch { }
        }

        private void сотрудникToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeClass EC = new EmployeeClass();
            EC.MdiParent = this;
            EC.Show();
        }

        private void журналЗаявокToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = моиЗаявкиToolStripMenuItem.Text;
        }

        private void журналЗаявокToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void администраторToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = администраторToolStripMenuItem.Text;
        }

        private void администраторToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void оПрограммеToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.ShowDialog(this);
        }

        private void списокСотрудниковToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            StaffReport SR = new StaffReport();
            SR.MdiParent = this;
            SR.Dispose();
            //SR.Show();
        }

        private void заявкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TaskTypes Tt = new TaskTypes();
            Tt.MdiParent = this;
            Tt.Show();
        }

        private void заявкиToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = заявкиToolStripMenuItem.Text;
        }

        private void заявкиToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void списокСотрудниковToolStripMenuItem1_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = списокСотрудниковToolStripMenuItem1.Text;
        }

        private void списокСотрудниковToolStripMenuItem1_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void новаяЗаявкаToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = новаяЗаявкаToolStripMenuItem.Text;
        }

        private void новаяЗаявкаToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void клиентыToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = клиентыToolStripMenuItem.Text;
        }

        private void клиентыToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void учетнаяКарточкаToolStripMenuItem1_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = учетнаяКарточкаToolStripMenuItem1.Text;
        }

        private void учетнаяКарточкаToolStripMenuItem1_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void инструкцияToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = инструкцияToolStripMenuItem.Text;
        }

        private void инструкцияToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void оПрограммеToolStripMenuItem1_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = оПрограммеToolStripMenuItem1.Text;
        }

        private void оПрограммеToolStripMenuItem1_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void сотрудникToolStripMenuItem1_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = сотрудникToolStripMenuItem1.Text;
        }

        private void сотрудникToolStripMenuItem1_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void журналЗаявокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyTasks MT = new MyTasks(personName_label1.Text);
            MT.MdiParent = this;
            MT.Show();
        }

        private void журналЗаявокToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            TasksJournal TJ = new TasksJournal();
            TJ.MdiParent = this;
            TJ.Show();
        }

        private void журналЗаявокToolStripMenuItem1_MouseEnter(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = журналЗаявокToolStripMenuItem1.Text;
        }

        private void журналЗаявокToolStripMenuItem1_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = " ";
        }

        private void новаяЗаявкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewTask NT = new NewTask(personName_label1.Text);
            NT.MdiParent = this;
            NT.Show();
        }

        private void клиентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clients clients = new Clients(false);
            clients.MdiParent = this;
            clients.Show();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
            Process process = new Process();
            process.Close();
        }

        private void гистограммаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Gistogramma Gist = new Gistogramma();
            Gist.MdiParent = this;
            Gist.Show();
        }
    }
}
