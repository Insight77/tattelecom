﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattelcom_Project
{
    class MyTasks : Form
    {
        private string personName;
        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
        private string selectCommand;

        private string[,] undoMode_saveInfo = new string[50, 20]; // сохранение информации
        private int numberStep = -1; // номер сохранения информации
        private bool fastCancel = false;

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        DataTable dataTable = new DataTable();
        private Button search_button1;
        private TextBox search_textBox1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem удалитьЗаявкуToolStripMenuItem;
        private Button undo_button1;
        private DataGridView dataGridView1;

        public MyTasks(string personName)
        {
            this.personName = personName;
            loadSelectCommand(ref selectCommand);
            InitializeComponent();
            dataBase.Open();
            loadDataGrid(selectCommand);
            dataGridViewConfiguration();
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.search_button1 = new System.Windows.Forms.Button();
            this.search_textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.удалитьЗаявкуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undo_button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 31);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(654, 426);
            this.dataGridView1.TabIndex = 0;
            // 
            // search_button1
            // 
            this.search_button1.Location = new System.Drawing.Point(591, 2);
            this.search_button1.Name = "search_button1";
            this.search_button1.Size = new System.Drawing.Size(75, 23);
            this.search_button1.TabIndex = 1;
            this.search_button1.Text = "Найти";
            this.search_button1.UseVisualStyleBackColor = true;
            this.search_button1.Click += new System.EventHandler(this.search_button1_Click);
            // 
            // search_textBox1
            // 
            this.search_textBox1.Location = new System.Drawing.Point(326, 4);
            this.search_textBox1.Name = "search_textBox1";
            this.search_textBox1.Size = new System.Drawing.Size(259, 20);
            this.search_textBox1.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьЗаявкуToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(677, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // удалитьЗаявкуToolStripMenuItem
            // 
            this.удалитьЗаявкуToolStripMenuItem.Name = "удалитьЗаявкуToolStripMenuItem";
            this.удалитьЗаявкуToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.удалитьЗаявкуToolStripMenuItem.Text = "Отменить заявку";
            this.удалитьЗаявкуToolStripMenuItem.Click += new System.EventHandler(this.удалитьЗаявкуToolStripMenuItem_Click);
            // 
            // undo_button1
            // 
            this.undo_button1.Enabled = false;
            this.undo_button1.Image = global::Tattelcom_Project.Properties.Resources.backImage;
            this.undo_button1.Location = new System.Drawing.Point(300, 5);
            this.undo_button1.Name = "undo_button1";
            this.undo_button1.Size = new System.Drawing.Size(20, 20);
            this.undo_button1.TabIndex = 4;
            this.undo_button1.UseVisualStyleBackColor = true;
            this.undo_button1.Click += new System.EventHandler(this.undo_button1_Click);
            // 
            // MyTasks
            // 
            this.ClientSize = new System.Drawing.Size(677, 462);
            this.Controls.Add(this.undo_button1);
            this.Controls.Add(this.search_textBox1);
            this.Controls.Add(this.search_button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MyTasks";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Мои заявки";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MyTasks_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private void loadDataGrid(string SQLcommand)
        {
            dataTable.Clear();
            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
        }

        private void dataGridViewConfiguration()
        {
            dataGridView1.Columns[0].Visible = false; // task_id
            dataGridView1.Columns[1].Width = 260; // вид услуги
            dataGridView1.Columns[2].Visible = false; // User FIO
            dataGridView1.Columns[3].Width = 200; // CLient FIO
            dataGridView1.Columns[4].Width = 90; // Client phone
            dataGridView1.Columns[5].Visible = false; // дата начала   
            dataGridView1.Columns[6].Width = 100; // дата завершения
            dataGridView1.Columns[7].Visible = false; // Tasks.user_id
            dataGridView1.Columns[8].Visible = false; // Tasks.type_id
            dataGridView1.Columns[9].Visible = false; // Tasks.clients_id

            dataGridView1.RowHeadersVisible = false;

            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);


            dataGridView1.ReadOnly = true;
        }

        private void loadSelectCommand(ref string selectCommand)
        {
            selectCommand = "SELECT task_id, Task_types.type_name, Users.fullname, Clients.fullname, Clients.phone,  date_begin, date_end, Tasks.user_id, Tasks.type_id, Tasks.client_id " +
                                       "FROM Tasks, Users, Task_types, Clients " +
                                       "WHERE Tasks.user_id = Users.id AND Tasks.type_id = Task_types.task_type_id AND Tasks.client_id = Clients.client_id " +
                                       " AND Users.fullname LIKE '" + personName + "'";
        }

        private void search_button1_Click(object sender, EventArgs e)
        {
            //поисковая строка 
            string word = search_textBox1.Text;

            //строка запроса с фильтром 
            string queryString = "SELECT task_id, Task_types.type_name, Users.fullname, Clients.fullname, Clients.phone, date_begin, date_end, Tasks.user_id, Tasks.type_id, Tasks.client_id" +
                                       " FROM Tasks, Users, Task_types, Clients" +
                                       " WHERE Tasks.user_id = Users.id AND Tasks.type_id = Task_types.task_type_id AND Tasks.client_id = Clients.client_id AND Users.fullname LIKE '" + personName + "' " +
                                       " AND (Clients.fullname LIKE '%" + word + "%'" +
                                       " OR Clients.phone LIKE '%" + word + "%'" +
                                       " OR Task_types.type_name LIKE '%" + word + "%'" +
                                       " OR date_end LIKE '%" + word + "%')";

            //вызов метода для заполнения грида с данными найденными по  фильтру 
            loadDataGrid(queryString);

            undo_button1.Enabled = true;
            fastCancel = true;
        }

        private void удалитьЗаявкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedColumn = dataGridView1.CurrentRow.Index; // Выделенная строка
                saveInfo(selectedColumn);

                string SQLQuery = string.Format("DELETE FROM [Tasks] WHERE [task_id]={0}",
                    dataGridView1[0, selectedColumn].Value.ToString()
                );

                try
                {
                    UpdateDataBase(SQLQuery);
                    MessageBox.Show("Заявка удалена", " Успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                undo_button1.Enabled = true;
                fastCancel = false;
            }
            catch { }
        }

        private void UpdateDataBase(string SQLQuery)
        {
            try
            {
                dataCommand.CommandText = SQLQuery;
                dataCommand.Connection = dataBase;
                dataAdapter.SelectCommand = dataCommand;
                dataCommand.ExecuteNonQuery(); // Проверка на изменение строк

                loadDataGrid(selectCommand);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void undo_button1_Click(object sender, EventArgs e)
        {
            if (fastCancel)
            {
                loadDataGrid(selectCommand);
                fastCancel = false;
            }
            else
            {
                string SQLQuery = string.Format("INSERT INTO [Tasks] ([task_id], [user_id], [client_id], [type_id], [date_begin], [date_end]) " +
                                            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')",
                    undoMode_saveInfo[numberStep, 0],
                    undoMode_saveInfo[numberStep, 1],
                    undoMode_saveInfo[numberStep, 2],
                    undoMode_saveInfo[numberStep, 3],
                    undoMode_saveInfo[numberStep, 4],
                    undoMode_saveInfo[numberStep, 5]
                );

                UpdateDataBase(SQLQuery);
            }
            

            numberStep--;
            if (numberStep == -1) undo_button1.Enabled = false;
        }

        private void saveInfo(int selectedColumn)
        {
            numberStep++;

            // MessageBox.Show(dataGridView1[0, selectedColumn].Value.ToString());
            undoMode_saveInfo[numberStep, 0] = dataGridView1[0, selectedColumn].Value.ToString();
            undoMode_saveInfo[numberStep, 1] = dataGridView1[7, selectedColumn].Value.ToString();
            undoMode_saveInfo[numberStep, 2] = dataGridView1[9, selectedColumn].Value.ToString();
            undoMode_saveInfo[numberStep, 3] = dataGridView1[8, selectedColumn].Value.ToString();
            undoMode_saveInfo[numberStep, 4] = dataGridView1[5, selectedColumn].Value.ToString();
            undoMode_saveInfo[numberStep, 5] = dataGridView1[6, selectedColumn].Value.ToString();
        }

        private void MyTasks_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataBase.Close();
        }
    }
}