﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;
using System.Runtime.InteropServices;
using System.Drawing;
using System.ComponentModel;

namespace Tattelcom_Project
{
    class TaskTypes : Form
    {
        /*//_______________________________Запрещение перемещение формы__________________________________//

        const int SC_CLOSE = 0xF010;
        const int MF_BYCOMMAND = 0;
        const int WM_NCLBUTTONDOWN = 0x00A1;
        const int WM_NCHITTEST = 0x0084;
        
        const int HTCAPTION = 2;
        [DllImport("User32.dll")]
        static extern int SendMessage(IntPtr hWnd,
        int Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("User32.dll")]
        static extern bool RemoveMenu(IntPtr hMenu, int uPosition, int uFlags);

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_NCLBUTTONDOWN)
            {
                int result = SendMessage(m.HWnd, WM_NCHITTEST,
                IntPtr.Zero, m.LParam);
                if (result == HTCAPTION)
                    return;
            }
            base.WndProc(ref m);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            IntPtr hMenu = GetSystemMenu(Handle, false);
            RemoveMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
        }

        //___________________________________________________________________________________________// */

        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
        private string selectCommand = "SELECT task_type_id, type_name FROM Task_types";
        private bool editMode = false;
        private int lastCreate_id = -1;
        private List<int> undoMode = new List<int>(100); // вид действия, которое надо отменить
        private string[,] undoMode_saveInfo = new string[100, 10]; // сохранение информации
        private int numberStep = -1; // номер сохранения информации
        private bool validUndo = true; // прошла ли отмена
        private bool fastCancel = false;
        private int fastCancel_id = -1;

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        DataTable dataTable = new DataTable();

        ToolTip toolTip = new ToolTip();

        private ToolStripMenuItem отменитьToolStripMenuItem;
        private ToolStripMenuItem новаяУслугаToolStripMenuItem;
        private ToolStripMenuItem удалитьУслугуToolStripMenuItem;
        private TextBox task_type_textBox1;
        private Button save_button1;
        private Panel panelCreate;
        private Button search_button1;
        private TextBox search_textBox1;
        private Button back_button1;
        private MenuStrip menuStrip1;
        private DataGridView dataGridView1;

        public TaskTypes()
        {
            InitializeComponent();
            dataBase.Open();
            loadDataGrid(selectCommand);
            lastCreate_id = dataGridView1.RowCount - 1;
            dataGridViewConfiguration();
            panelCreate.Enabled = false;
            back_button1.Enabled = false;
            undoMode.Add(-1);    
        }

        // Настройки dataGridView1
        

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.новаяУслугаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьУслугуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.task_type_textBox1 = new System.Windows.Forms.TextBox();
            this.save_button1 = new System.Windows.Forms.Button();
            this.panelCreate = new System.Windows.Forms.Panel();
            this.search_button1 = new System.Windows.Forms.Button();
            this.search_textBox1 = new System.Windows.Forms.TextBox();
            this.back_button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panelCreate.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(5, 68);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(271, 304);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяУслугаToolStripMenuItem,
            this.удалитьУслугуToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(282, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // новаяУслугаToolStripMenuItem
            // 
            this.новаяУслугаToolStripMenuItem.Name = "новаяУслугаToolStripMenuItem";
            this.новаяУслугаToolStripMenuItem.Size = new System.Drawing.Size(112, 20);
            this.новаяУслугаToolStripMenuItem.Text = "Новый вид услуг";
            this.новаяУслугаToolStripMenuItem.Click += new System.EventHandler(this.новаяУслугаToolStripMenuItem_Click);
            // 
            // удалитьУслугуToolStripMenuItem
            // 
            this.удалитьУслугуToolStripMenuItem.Name = "удалитьУслугуToolStripMenuItem";
            this.удалитьУслугуToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.удалитьУслугуToolStripMenuItem.Text = "Удалить вид  услуг";
            this.удалитьУслугуToolStripMenuItem.Click += new System.EventHandler(this.удалитьУслугуToolStripMenuItem_Click);
            // 
            // task_type_textBox1
            // 
            this.task_type_textBox1.Location = new System.Drawing.Point(6, 5);
            this.task_type_textBox1.Name = "task_type_textBox1";
            this.task_type_textBox1.Size = new System.Drawing.Size(181, 20);
            this.task_type_textBox1.TabIndex = 2;
            // 
            // save_button1
            // 
            this.save_button1.Location = new System.Drawing.Point(193, 3);
            this.save_button1.Name = "save_button1";
            this.save_button1.Size = new System.Drawing.Size(75, 23);
            this.save_button1.TabIndex = 3;
            this.save_button1.Text = "Сохранить";
            this.save_button1.UseVisualStyleBackColor = true;
            this.save_button1.Click += new System.EventHandler(this.save_button1_Click);
            // 
            // panelCreate
            // 
            this.panelCreate.Controls.Add(this.save_button1);
            this.panelCreate.Controls.Add(this.task_type_textBox1);
            this.panelCreate.Location = new System.Drawing.Point(5, 378);
            this.panelCreate.Name = "panelCreate";
            this.panelCreate.Size = new System.Drawing.Size(271, 29);
            this.panelCreate.TabIndex = 4;
            // 
            // search_button1
            // 
            this.search_button1.Location = new System.Drawing.Point(198, 37);
            this.search_button1.Name = "search_button1";
            this.search_button1.Size = new System.Drawing.Size(75, 23);
            this.search_button1.TabIndex = 5;
            this.search_button1.Text = "Найти";
            this.search_button1.UseVisualStyleBackColor = true;
            this.search_button1.Click += new System.EventHandler(this.search_button1_Click);
            // 
            // search_textBox1
            // 
            this.search_textBox1.Location = new System.Drawing.Point(5, 39);
            this.search_textBox1.Name = "search_textBox1";
            this.search_textBox1.Size = new System.Drawing.Size(187, 20);
            this.search_textBox1.TabIndex = 4;
            // 
            // back_button1
            // 
            this.back_button1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.back_button1.Image = global::Tattelcom_Project.Properties.Resources.backImage;
            this.back_button1.Location = new System.Drawing.Point(256, 4);
            this.back_button1.Name = "back_button1";
            this.back_button1.Size = new System.Drawing.Size(20, 20);
            this.back_button1.TabIndex = 7;
            this.back_button1.UseVisualStyleBackColor = true;
            this.back_button1.Click += new System.EventHandler(this.back_button1_Click);
            // 
            // TaskTypes
            // 
            this.ClientSize = new System.Drawing.Size(282, 411);
            this.Controls.Add(this.back_button1);
            this.Controls.Add(this.search_button1);
            this.Controls.Add(this.search_textBox1);
            this.Controls.Add(this.panelCreate);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TaskTypes";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Виды услуг";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TaskTypes_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelCreate.ResumeLayout(false);
            this.panelCreate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void dataGridViewConfiguration()
        {
            dataGridView1.Columns[0].Visible = false; // id
            dataGridView1.Columns[1].Width = 260; // Вид услуг
            dataGridView1.RowHeadersVisible = false;


            dataGridView1.ReadOnly = true;

            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);

            toolTip.SetToolTip(back_button1, "Отменить");
        }
    
        // Загрузка данных в dataGridView1
        private void loadDataGrid(string SQLcommand)
        {
            dataTable.Clear();
            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
        }

        private void удалитьУслугуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedColumn = dataGridView1.CurrentRow.Index; // Выделенная строка
                undoMode.Add(2); // Добавление шага удаления
                saveInfo(selectedColumn);

                string SQLQuery = string.Format("DELETE FROM [Task_types] WHERE [task_type_id]={0}",
                    dataGridView1[0, selectedColumn].Value.ToString()
                );

                try
                {
                    UpdateDataBase(SQLQuery);
                    MessageBox.Show("Запись удалена", " успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                clearInputs();
                dataGridView1.Enabled = true;
                panelCreate.Enabled = false;

                back_button1.Enabled = true;
                fastCancel = false;
            }
            catch { }       
        }

        private void новаяУслугаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clearInputs();
            panelCreate.Enabled = true;
            dataGridView1.Enabled = false;
            editMode = false;
            
            back_button1.Enabled = true;
            fastCancel = true;
            fastCancel_id = 0;        
        }

        private void clearInputs()
        {
            try
            {
                dataGridView1.CurrentRow.Selected = false;
            }
            catch { }

            task_type_textBox1.Clear();
        }

        private void save_button1_Click(object sender, EventArgs e)
        {
            if (validInfo())
            {
                string SQLQuery;

                if (editMode)
                {
                    int selectedColumn = dataGridView1.CurrentRow.Index;
                    undoMode.Add(0); // Добавление шага редактирования
                    saveInfo(selectedColumn);

                    SQLQuery = string.Format("UPDATE [Task_types] SET [type_name]='{1}' WHERE [task_type_id]={0}",
                        dataGridView1[0, selectedColumn].Value.ToString(),
                        task_type_textBox1.Text
                    );
                }
                else
                {
                    SQLQuery = string.Format("INSERT INTO [Task_types] ([task_type_id], [type_name]) VALUES ('{0}', '{1}')",
                        ++lastCreate_id,
                        task_type_textBox1.Text
                    );

                    undoMode.Add(1); // Добавление шага сохранения новой записи
                    saveInfo(lastCreate_id);
                }

                try
                {
                    UpdateDataBase(SQLQuery);
                    MessageBox.Show("Запись сохранена", " успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                panelCreate.Enabled = false;
                clearInputs();
                dataGridView1.Enabled = true;


                fastCancel = false;
                back_button1.Enabled = true;
            }                   
        }

        private void UpdateDataBase(string SQLQuery)
        {
            try
            {
                dataCommand.CommandText = SQLQuery;
                dataCommand.Connection = dataBase;
                dataAdapter.SelectCommand = dataCommand;
                dataCommand.ExecuteNonQuery(); // Проверка на изменение строк

                loadDataGrid(selectCommand);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int selectRow = dataGridView1.CurrentRow.Index; // Выделенная строка

            if (selectRow != dataGridView1.Rows[dataGridView1.RowCount - 1].Index)
            {
                task_type_textBox1.Text = dataGridView1[1, selectRow].Value.ToString();

                panelCreate.Enabled = true;
                editMode = true;
            }
            else
            {
                clearInputs();
                panelCreate.Enabled = false;
                editMode = false;
            }
            
        }

        private void search_button1_Click(object sender, EventArgs e)
        {
            //поисковая строка 
            string word = search_textBox1.Text;

            //строка запроса с фильтром 
            string queryString = "SELECT [task_type_id], [type_name] FROM [Task_types] WHERE [type_name] LIKE '%" + word + "%'";

            //вызов метода для заполнения грида с данными найденными по  фильтру 
            clearInputs();
            loadDataGrid(queryString);

            
            back_button1.Enabled = true;
            fastCancel = true;
            fastCancel_id = 1;        
        }

        private void back_button1_Click(object sender, EventArgs e)
        {
            if (fastCancel)
            {
                 // Отменить создание записи
                  if (fastCancel_id == 0)
                  {
                      clearInputs();
                      panelCreate.Enabled = false;
                      dataGridView1.Enabled = true;
                  }

                  if (fastCancel_id == 1)
                  {
                      loadDataGrid(selectCommand);
                  }

                  fastCancel = false; 
            }
            else
            {
                // Отменить редактирование записи
                if ((undoMode.Last() == 0) && validUndo)
                {
                    //MessageBox.Show(undoMode.Last().ToString());
                    string SQLQuery = string.Format("UPDATE [Task_types] SET [type_name]='{1}' WHERE [task_type_id]={0}",
                        undoMode_saveInfo[numberStep, 0],
                        undoMode_saveInfo[numberStep, 1]
                    );

                    UpdateDataBase(SQLQuery);

                    deleteInfo();
                    validUndo = false;
                }

                // Отменить сохранение новой записи
                if ((undoMode.Last() == 1) && validUndo)
                {
                    //MessageBox.Show(undoMode_saveInfo[numberStep, 0]);
                    string SQLQuery = string.Format("DELETE FROM [Task_types] WHERE [task_type_id]={0}",
                        undoMode_saveInfo[numberStep, 0]
                    );

                    UpdateDataBase(SQLQuery);

                    deleteInfo();
                    validUndo = false;

                }

                // Отменить удаление записи
                if ((undoMode.Last() == 2) && validUndo)
                {
                    //MessageBox.Show(undoMode.Last().ToString());
                    string SQLQuery = string.Format("INSERT INTO [Task_types] ([task_type_id], [type_name]) VALUES ('{0}', '{1}')",
                        undoMode_saveInfo[numberStep, 0],
                        undoMode_saveInfo[numberStep, 1]
                    );

                    UpdateDataBase(SQLQuery);

                    deleteInfo();
                    validUndo = false;
                }
            }
            if (undoMode.Last() == -1) back_button1.Enabled = false;
            validUndo = true;
        }

        private void saveInfo(int selectedColumn)
        {
            numberStep++;
            if (undoMode.Last() != 1)
            {
                // MessageBox.Show(dataGridView1[0, selectedColumn].Value.ToString());
                undoMode_saveInfo[numberStep, 0] = dataGridView1[0, selectedColumn].Value.ToString();
                undoMode_saveInfo[numberStep, 1] = dataGridView1[1, selectedColumn].Value.ToString();
            }
            else
                undoMode_saveInfo[numberStep, 0] = selectedColumn.ToString();
        }

        private void deleteInfo()
        {
            numberStep--;
            undoMode.RemoveAt(undoMode.Count - 1);        
        }

        private bool validInfo()
        {
            bool matches = false;

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                if (task_type_textBox1.Text == dataGridView1[1, i].Value.ToString())
                    matches = true;

            if (matches)
            {
                MessageBox.Show("Такой вид услуг уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (task_type_textBox1.Text == "")
            {
                MessageBox.Show("Поле является пустым!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
    
            else return true;              
        }

        private void TaskTypes_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataBase.Close();
        }
    }
}
