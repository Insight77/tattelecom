﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattelcom_Project
{
    class Universal : Form
    {       
        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";       

        private string SQLQuery;
        private string selectCommand;
        private string fromString;
        private int lastCreateId;
        private string[,] saveInfo;
        private int numberStep;

        private List<int> undoMode = new List<int>(100);

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        private DataGridView uniDataGridView;
        DataTable dataTable = new DataTable();
        DataTable[] dataTables = new DataTable[100];
        int test = -1;

        public Universal(string selectCommand)
        {
            InitializeComponent();
            this.selectCommand = selectCommand;           
            dataBase.Open();
            dataCommand.CommandText = selectCommand;
            dataCommand.Connection = dataBase;
            loadDataGrid(uniDataGridView);
            this.lastCreateId = uniDataGridView.RowCount - 1;
            this.saveInfo = new string[100, uniDataGridView.RowCount];
            this.numberStep = -1;
            undoMode.Add(-1);
            FindFromString();
        }

        public void loadDataGrid(DataGridView dataGridView)
        {
            dataTable.Clear();
            dataCommand.CommandText = selectCommand;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public void Remove(DataGridView dataGridView)
        {
            try
            {
                int selectedColumn = dataGridView.CurrentRow.Index; // Выделенная строка
                /*undoMode.Add(2); // Добавление шага удаления
                saveInfo(selectedColumn);*/               
                SQLQuery = string.Format("DELETE FROM [" + fromString + "] WHERE [" + uniDataGridView.Columns[0].Name + "]={0}",
                    dataGridView[0, selectedColumn].Value.ToString()
                );

                try
                {
                    //UpdateDataBase(uniDataGridView, SQLQuery);
                    UpdateDataBase(dataGridView, SQLQuery);
                    MessageBox.Show("Запись удалена", " успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                /*clearInputs();
                dataGridView1.Enabled = true;
                panelCreate.Enabled = false;

                back_button1.Enabled = true;
                fastCancel = false;*/
            }
            catch { }
        }

        private void FindFromString()
        {
            int firstIndex = selectCommand.LastIndexOf('[') + 1;
            int secondIndex = selectCommand.LastIndexOf(']');
            fromString = selectCommand.Substring(firstIndex, secondIndex - firstIndex);           
        }

        private void UpdateDataBase(DataGridView dataGridView, string SQLQuery)
        {
            try
            {
                dataCommand.CommandText = SQLQuery;
                dataAdapter.SelectCommand = dataCommand;
                dataCommand.ExecuteNonQuery(); // Проверка на изменение строк
                loadDataGrid(dataGridView);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Save(DataGridView dataGridView, bool editMode, TextBox[] textBoxs, Panel panelCreate)
        {
            test++;
            dataTables[test] = (DataTable)dataGridView.DataSource;

            MessageBox.Show("1");
            if (editMode)
            {
                //undoMode.Add(0);
                SaveInfo(dataGridView);
                Edit(dataGridView, textBoxs);
            }
            else
            {
                New(dataGridView, textBoxs);
            }

            dataGridView.Enabled = true;
            panelCreate.Enabled = false;
            ClearTextBoxs(textBoxs);

            MessageBox.Show("Запись сохранена", " успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information);

            /*panelCreate.Enabled = false;
            clearInputs();
            dataGridView1.Enabled = true;


            fastCancel = false;
            back_button1.Enabled = true;*/
        }

        private void Edit(DataGridView dataGridView, TextBox[] textBoxs)
        {
            try
            {              
                int selectedColumn = dataGridView.CurrentRow.Index;

                for (int i = 1; i < dataGridView.ColumnCount; i++)
                {
                    SQLQuery = string.Format("UPDATE [" + fromString + "] SET [" + dataGridView.Columns[i].Name + "]='{1}' " +
                                             "WHERE [" + dataGridView.Columns[0].Name + "]={0}",
                        dataGridView[0, selectedColumn].Value.ToString(),
                        textBoxs[i - 1].Text
                    );
                    MessageBox.Show("2");
                    //UpdateDataBase(uniDataGridView, SQLQuery);
                    UpdateDataBase(dataGridView, SQLQuery);
                }

                
            }
            catch (Exception ex) { MessageBox.Show(ex.Message + "Edit()"); }
                                      
            /*undoMode.Add(0); // Добавление шага редактирования
            saveInfo(selectedColumn);*/           
        }

        private void New(DataGridView dataGridView, TextBox[] textBoxs)
        {
            SQLQuery = string.Format("INSERT INTO [" + fromString + "] ([" + dataGridView.Columns[0].Name + "]) " +
                                         "VALUES ('{0}')",
                    ++lastCreateId
                );

            //UpdateDataBase(uniDataGridView, SQLQuery);
            UpdateDataBase(dataGridView, SQLQuery);

            for (int i = 1; i < dataGridView.ColumnCount; i++)
            {
                SQLQuery = string.Format("UPDATE [" + fromString + "] SET [" + dataGridView.Columns[i].Name + "]='{1}' " +
                                     "WHERE [" + dataGridView.Columns[0].Name + "]={0}",
                    lastCreateId,
                    textBoxs[i - 1].Text
                );

                //UpdateDataBase(uniDataGridView, SQLQuery);
                UpdateDataBase(dataGridView, SQLQuery);
            }

            /*undoMode.Add(1); // Добавление шага сохранения новой записи
            saveInfo(lastCreate_id);*/
        }

        public void DataGridConfiguration(DataGridView dataGridView)
        {
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridView.ReadOnly = true;
        }

        public void DataGridCellClick(DataGridView dataGridView, TextBox[] textBoxs, Panel panelCreate)
        {
            int selectedRow = dataGridView.CurrentRow.Index; // Выделенная строка
            if (selectedRow != dataGridView.Rows[dataGridView.RowCount - 1].Index)
            {
                for(int i = 0; i < textBoxs.Length; i++)
                {
                    textBoxs[i].Text = dataGridView[i + 1, selectedRow].Value.ToString();
                }

                panelCreate.Enabled = true;
                //editMode = true;
            }
            else
            {
                /*clearInputs();
                panelCreate.Enabled = false;
                editMode = false;*/
            }
        }
        
        public void CreateButton(DataGridView dataGridView, TextBox[] textBoxs, Panel panelCreate)
        {
            dataGridView.CurrentRow.Selected = false;
            dataGridView.Enabled = false;
            panelCreate.Enabled = true;
            ClearTextBoxs(textBoxs);
            textBoxs[0].Focus();
        }

        public void Search(DataGridView dataGridView, TextBox searchBox)
        {
            string word = searchBox.Text;
            string replaceSelectCommand;

            replaceSelectCommand = selectCommand;
            //строка запроса с фильтром 
            selectCommand += " WHERE [" + uniDataGridView.Columns[1].Name + "] LIKE '%" + word + "%'";

            //вызов метода для заполнения грида с данными найденными по  фильтру 
            //clearInputs();
            
            MessageBox.Show(selectCommand);
            loadDataGrid(dataGridView);
            selectCommand = replaceSelectCommand;

            /*back_button1.Enabled = true;
            fastCancel = true;
            fastCancel_id = 1;*/
        }

        private void ClearTextBoxs(TextBox[] textBoxs)
        {
            for (int i = 0; i < textBoxs.Length; i++)
                textBoxs[i].Clear();
        }

        public void Undo(DataGridView dataGridView)
        {
            OleDbCommandBuilder builder = new OleDbCommandBuilder(dataAdapter);            
            
            dataAdapter.Update(dataTables[test]);
            dataCommand.ExecuteNonQuery();
            loadDataGrid(dataGridView);
                        
            if(undoMode.Last() == 0)
            {
                UndoEdit(dataGridView);
            }
        }

        private void UndoEdit(DataGridView dataGridView)
        {
            int selectedColumn = numberStep;

            for (int i = 1; i < dataGridView.ColumnCount; i++)
            {
                SQLQuery = string.Format("UPDATE [" + fromString + "] SET [" + dataGridView.Columns[i].Name + "]='{1}' " +
                                         "WHERE [" + dataGridView.Columns[0].Name + "]={0}",
                saveInfo[numberStep, 0],
                saveInfo[numberStep, i]
                );

                //UpdateDataBase(uniDataGridView, SQLQuery);
                UpdateDataBase(dataGridView, SQLQuery);
            }

            deleteInfo();
        }

        private void SaveInfo(DataGridView dataGridView)
        {
            numberStep++;
            int selectedRow = dataGridView.CurrentRow.Index;
            for (int i = 0; i < dataGridView.ColumnCount; i++)
            {
                saveInfo[numberStep, i] = dataGridView[i, selectedRow].Value.ToString();
            }
                
        }

        private void deleteInfo()
        {
            numberStep--;
            undoMode.RemoveAt(undoMode.Count - 1);
        }

        private void InitializeComponent()
        {
            this.uniDataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.uniDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // uniDataGridView
            // 
            this.uniDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.uniDataGridView.Location = new System.Drawing.Point(13, 12);
            this.uniDataGridView.Name = "uniDataGridView";
            this.uniDataGridView.Size = new System.Drawing.Size(463, 392);
            this.uniDataGridView.TabIndex = 0;
            // 
            // Universal
            // 
            this.ClientSize = new System.Drawing.Size(488, 416);
            this.Controls.Add(this.uniDataGridView);
            this.Name = "Universal";
            ((System.ComponentModel.ISupportInitialize)(this.uniDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

    }
}
