﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattelcom_Project
{
    class NewTask : Form
    {
        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
        // private string selectCommand = "SELECT task_type_id, type_name, Users.id, Users.fullname, Clients.client_id, Clients.fullname FROM Task_types, Users, Clients";

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        DataTable dataTableComboBox = new DataTable();
        DataTable dataTableUsers = new DataTable();
        DataTable dataTableClients = new DataTable();
        DataTable dataTableNewClients = new DataTable();
        DataTable dataTableValidInfo = new DataTable();

        private int idCurrentUser = -1; // id текущего сотрудника
        private int idCurrentClient = -1; // id текущего клиента
        static DateTime today = new DateTime();
        private string todayDayString = today.ToShortDateString();
        private string personNameCurrentUser;

        private TextBox clientFullname_textBox1;
        private TextBox clientPassport_textBox1;
        private TextBox clientAddress_textBox1;
        private TextBox clientPhone_textBox1;
        private ComboBox taskType_comboBox1;
        private Button button1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Button button2;
        private DateTimePicker dateTimePicker1;
        private Label label6;

        public NewTask(string personNameCurrentUser)
        {
            this.personNameCurrentUser = personNameCurrentUser;
            InitializeComponent();
            dataBase.Open();                     
            taskTypesConfiguration();
            clientPhone_textBox1.MaxLength = 11;
            clientPhone_textBox1.Text = "8";
        }

        private void InitializeComponent()
        {
            this.clientFullname_textBox1 = new System.Windows.Forms.TextBox();
            this.clientPassport_textBox1 = new System.Windows.Forms.TextBox();
            this.clientAddress_textBox1 = new System.Windows.Forms.TextBox();
            this.clientPhone_textBox1 = new System.Windows.Forms.TextBox();
            this.taskType_comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // clientFullname_textBox1
            // 
            this.clientFullname_textBox1.Location = new System.Drawing.Point(167, 26);
            this.clientFullname_textBox1.Name = "clientFullname_textBox1";
            this.clientFullname_textBox1.Size = new System.Drawing.Size(183, 20);
            this.clientFullname_textBox1.TabIndex = 0;
            // 
            // clientPassport_textBox1
            // 
            this.clientPassport_textBox1.Location = new System.Drawing.Point(167, 52);
            this.clientPassport_textBox1.Name = "clientPassport_textBox1";
            this.clientPassport_textBox1.Size = new System.Drawing.Size(121, 20);
            this.clientPassport_textBox1.TabIndex = 1;
            // 
            // clientAddress_textBox1
            // 
            this.clientAddress_textBox1.Location = new System.Drawing.Point(167, 78);
            this.clientAddress_textBox1.Name = "clientAddress_textBox1";
            this.clientAddress_textBox1.Size = new System.Drawing.Size(216, 20);
            this.clientAddress_textBox1.TabIndex = 2;
            // 
            // clientPhone_textBox1
            // 
            this.clientPhone_textBox1.Location = new System.Drawing.Point(167, 104);
            this.clientPhone_textBox1.Name = "clientPhone_textBox1";
            this.clientPhone_textBox1.Size = new System.Drawing.Size(121, 20);
            this.clientPhone_textBox1.TabIndex = 3;
            // 
            // taskType_comboBox1
            // 
            this.taskType_comboBox1.FormattingEnabled = true;
            this.taskType_comboBox1.Location = new System.Drawing.Point(167, 130);
            this.taskType_comboBox1.Name = "taskType_comboBox1";
            this.taskType_comboBox1.Size = new System.Drawing.Size(216, 21);
            this.taskType_comboBox1.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(167, 185);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(216, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Новая заявка";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Фамилия Имя Отчество:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Серия Номер:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Адрес:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Телефон:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Вид предоставляемых услуг:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Дата окончания:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(356, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(27, 25);
            this.button2.TabIndex = 13;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(167, 157);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(121, 20);
            this.dateTimePicker1.TabIndex = 14;
            // 
            // NewTask
            // 
            this.ClientSize = new System.Drawing.Size(395, 215);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.taskType_comboBox1);
            this.Controls.Add(this.clientPhone_textBox1);
            this.Controls.Add(this.clientAddress_textBox1);
            this.Controls.Add(this.clientPassport_textBox1);
            this.Controls.Add(this.clientFullname_textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewTask";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Новая заявка";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NewTask_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void loadData(DataTable dataTable, string SQLcommand)
        {
            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
        }

        private void taskTypesConfiguration()
        {
            string SQLcommand = "SELECT [Task_types.type_name] FROM [Task_Types]";

            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTableComboBox);

            foreach (DataRow row in dataTableComboBox.Rows)
            {
                taskType_comboBox1.Items.Add(row[0].ToString());
            }

            // comboBoxConfig 
            taskType_comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            try { taskType_comboBox1.SelectedIndex = 0; }
            catch { }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (validInfo())
            {
                if (validNewClient())
                {
                    string SQLQueryClient = string.Format("INSERT INTO [Clients] ([fullname], [passport_number], [address], [phone]) VALUES ('{0}', '{1}', '{2}', '{3}')",
                            clientFullname_textBox1.Text,
                            clientPassport_textBox1.Text,
                            clientAddress_textBox1.Text,
                            clientPhone_textBox1.Text
                );

                    try
                    {
                        UpdateDataBase(SQLQueryClient);
                        // MessageBox.Show("ClientUpdate");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }


                findIdCurrentUser(); // Находим id текущего сотрудника   
                findIdCurrentClient(); // Находим id текущего клиента

                if (validNewTask())
                {
                    string SQLQueryTasks = string.Format("INSERT INTO [Tasks] ([user_id], [client_id], [type_id], [date_begin], [date_end], [date_begin_hour]) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', {5})",
                            idCurrentUser,
                            idCurrentClient,
                            (taskType_comboBox1.SelectedIndex + 1),
                            todayDayString,
                            dateTimePicker1.Value.ToShortDateString(),
                            DateTime.Now.Hour.ToString()
                );

                    try
                    {
                        UpdateDataBase(SQLQueryTasks);
                        if (MessageBox.Show("Запись сохранена", " успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                            this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    /*clientFullname_textBox1.Clear();
                    clientPassport_textBox1.Clear();
                    clientAddress_textBox1.Clear();
                    clientPhone_textBox1.Clear(); */
                    //int countRow = int.Parse(dataGridView1.RowCount.ToString);             
                }

            }

        }

        private void UpdateDataBase(string SQLQuery)
        {
            try
            {
                dataCommand.CommandText = SQLQuery;
                dataCommand.Connection = dataBase;
                dataAdapter.SelectCommand = dataCommand;
                dataCommand.ExecuteNonQuery(); // Проверка на изменение строк
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void findIdCurrentUser()
        {
            string SQLcommand = "SELECT [Users.id], [Users.fullname] FROM [Users]";
            dataTableUsers.Clear();
            loadData(dataTableUsers, SQLcommand);

            foreach (DataRow row in dataTableUsers.Rows)
                if (personNameCurrentUser == (row[1].ToString()))
                {
                    idCurrentUser = (int)row[0];
                    break;
                }
        }
        
        private void findIdCurrentClient()
        {
            string personNameCurrentClient = clientFullname_textBox1.Text;

            string SQLcommand = "SELECT [Clients.client_id], [Clients.fullname] FROM [Clients]";
            dataTableClients.Clear();
            loadData(dataTableClients, SQLcommand);

            foreach (DataRow row in dataTableClients.Rows)
            {
                if (personNameCurrentClient == (row[1].ToString()))
                {
                    idCurrentClient = (int)row[0];
                    break;
                }
            }
                
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clients clients = new Clients(true);
            clients.ShowDialog();
            clientFullname_textBox1.Text = clients.clientFullName;
            clientPassport_textBox1.Text = clients.clientPassNumber;
            clientAddress_textBox1.Text = clients.clientAddress;
            clientPhone_textBox1.Text = clients.clientPhone;
        }

        private bool validNewClient()
        {
            string personNameCurrentClient = clientFullname_textBox1.Text;

            string SQLcommand = "SELECT [Clients.fullname] FROM [Clients]";

            loadData(dataTableNewClients, SQLcommand);

            foreach (DataRow row in dataTableNewClients.Rows)
                if (personNameCurrentClient == (row[0].ToString()))
                    return false;

            return true;
        }

        private bool validInfo()
        {
            if ((clientFullname_textBox1.Text == "") || (clientPassport_textBox1.Text == "") ||
                (clientAddress_textBox1.Text == "") || (clientPhone_textBox1.Text == ""))
            {                
                MessageBox.Show("Одно из полей является пустым!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool validNewTask()
        {
            string SQLcommand = "SELECT [client_id], [type_id] FROM [Tasks]";
            dataTableValidInfo.Clear();
            loadData(dataTableValidInfo, SQLcommand);

            foreach (DataRow row in dataTableValidInfo.Rows)
            {
                //MessageBox.Show("CurrentClient " + idCurrentClient.ToString());
                //MessageBox.Show("row[0] " + (row[0].ToString()));

                if (idCurrentClient.ToString() == (row[0].ToString()))
                {
                    //MessageBox.Show((taskType_comboBox1.SelectedIndex + 1).ToString());
                    //MessageBox.Show(row[1].ToString());
                    if ((taskType_comboBox1.SelectedIndex + 1).ToString() == row[1].ToString())
                    {
                        MessageBox.Show("Такая заявка уже существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                
            }

            return true;
        }
        

        private void NewTask_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataBase.Close();
        }
    }
}
