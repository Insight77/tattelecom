﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattelcom_Project
{
    class Test : Form
    {
        
        private DataGridView dataGridView1;


        private static string selectCommand = "SELECT [task_type_id], [type_name], [Цена услуги] FROM [Task_types]"; // Указать команду выборки
        private MenuStrip menuStrip1;
        private ToolStripMenuItem удалитьToolStripMenuItem;
        private TextBox textBox1;
        private Button button1;
        private static int lastCreateId;
        private static int numberTextBox = 2; // Указать кол-во полей
        private bool editMode = false;
        Universal uni = new Universal(selectCommand);
        private ToolStripMenuItem создатьToolStripMenuItem;
        private string[] newInfo = new string[numberTextBox];
        private Panel panelCreate;
        private TextBox textBox2;
        private Button button2;
        private TextBox textBox3;
        private Button button3;
        private mydbDataSet mydbDataSet;
        private BindingSource tasktypesBindingSource;
        private System.ComponentModel.IContainer components;
        private mydbDataSetTableAdapters.Task_typesTableAdapter task_typesTableAdapter;
        TextBox[] textBoxs = new TextBox[numberTextBox];  

        public Test()
        {
            InitializeComponent();
            uni.loadDataGrid(dataGridView1);
            uni.DataGridConfiguration(dataGridView1);
            textBoxs[0] = textBox1;
            textBoxs[1] = textBox3;
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.создатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panelCreate = new System.Windows.Forms.Panel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.mydbDataSet = new Tattelcom_Project.mydbDataSet();
            this.tasktypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.task_typesTableAdapter = new Tattelcom_Project.mydbDataSetTableAdapters.Task_typesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panelCreate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mydbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasktypesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 69);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(505, 307);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьToolStripMenuItem,
            this.удалитьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(529, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // создатьToolStripMenuItem
            // 
            this.создатьToolStripMenuItem.Name = "создатьToolStripMenuItem";
            this.создатьToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.создатьToolStripMenuItem.Text = "Создать";
            this.создатьToolStripMenuItem.Click += new System.EventHandler(this.создатьToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(3, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(285, 20);
            this.textBox1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(400, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelCreate
            // 
            this.panelCreate.Controls.Add(this.textBox3);
            this.panelCreate.Controls.Add(this.textBox1);
            this.panelCreate.Controls.Add(this.button1);
            this.panelCreate.Enabled = false;
            this.panelCreate.Location = new System.Drawing.Point(12, 382);
            this.panelCreate.Name = "panelCreate";
            this.panelCreate.Size = new System.Drawing.Size(505, 94);
            this.panelCreate.TabIndex = 4;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(294, 16);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 40);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(285, 20);
            this.textBox2.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(306, 37);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(412, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(24, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // mydbDataSet
            // 
            this.mydbDataSet.DataSetName = "mydbDataSet";
            this.mydbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tasktypesBindingSource
            // 
            this.tasktypesBindingSource.DataMember = "Task_types";
            this.tasktypesBindingSource.DataSource = this.mydbDataSet;
            // 
            // task_typesTableAdapter
            // 
            this.task_typesTableAdapter.ClearBeforeFill = true;
            // 
            // Test
            // 
            this.ClientSize = new System.Drawing.Size(529, 427);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.panelCreate);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Test";
            this.Load += new System.EventHandler(this.Test_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelCreate.ResumeLayout(false);
            this.panelCreate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mydbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasktypesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uni.Remove(dataGridView1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            uni.Save(dataGridView1, editMode, textBoxs, panelCreate);
        }

        private void FindLastCreateId()
        {
            lastCreateId = dataGridView1.RowCount - 1;
        }

        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editMode = false;
            uni.CreateButton(dataGridView1, textBoxs, panelCreate);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editMode = true;
            uni.DataGridCellClick(dataGridView1, textBoxs, panelCreate);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            uni.Search(dataGridView1, textBox2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            uni.Undo(dataGridView1);
        }

        private void Test_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "mydbDataSet.Task_types". При необходимости она может быть перемещена или удалена.
            this.task_typesTableAdapter.Fill(this.mydbDataSet.Task_types);

        }
    }
}
