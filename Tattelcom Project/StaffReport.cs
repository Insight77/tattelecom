﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;
using System.IO;
using System.Diagnostics;

namespace Tattelcom_Project
{
    class StaffReport : Form
    {
        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
        private string selectCommand = "SELECT fullname, Jobs.job_name, login, password, Users.job_id FROM Users, Jobs WHERE Users.job_id=Jobs.job_id";

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        private DataGridView dataGridView1;
        System.Data.DataTable dataTable = new System.Data.DataTable();
        System.Data.DataTable dataTableUsers = new System.Data.DataTable();
        System.Data.DataTable dataTableCountTasks = new System.Data.DataTable();
        private int countTasks = 0;
        private int[] countTasksMassive;

        SaveFileDialog saveFileDialog = new SaveFileDialog();
        Application excel = new Application();
        Workbook workBook;
        Worksheet worksheet;

        public StaffReport()
        {
            InitializeComponent();
            dataGridView1.Visible = false;

            saveFileDialog.Filter = "Книга Excel (*.xlsx)|*.xlsx";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    saveFile();
                    MessageBox.Show("Отчет успешно сохранен", " Успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                catch (Exception)
                {
                }                           
            }

        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(388, 363);
            this.dataGridView1.TabIndex = 0;
            // 
            // StaffReport
            // 
            this.ClientSize = new System.Drawing.Size(412, 387);
            this.Controls.Add(this.dataGridView1);
            this.Name = "StaffReport";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        private void loadDataGrid(string SQLcommand)
        {
            dataTable.Clear();
            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
        }

        private void saveFile()
        {
            loadDataGrid(selectCommand);
            string savedFileName = saveFileDialog.FileName;
            countTasksMassive = new int[dataGridView1.RowCount];

            workBook = excel.Workbooks.Add(System.Reflection.Missing.Value);
            worksheet = (Worksheet)workBook.Sheets[1];
            countTasksFromUser(ref countTasksMassive); // кол-во заявок

            /*for (int j = 0; j < dataGridView1.ColumnCount - 1; j++)
                worksheet.Cells[1, j + 1] = dataGridView1.Columns[j].HeaderText;*/

            for (int j = 1; j < dataGridView1.ColumnCount; j++)
                (worksheet.Cells[1, j] as Range).Font.Bold = true;
            (worksheet.Cells[1, dataGridView1.ColumnCount] as Range).Font.Bold = true;

            worksheet.Columns[1].ColumnWidth = 35;
            worksheet.Columns[2].ColumnWidth = 15;
            worksheet.Columns[3].ColumnWidth = 15;
            worksheet.Columns[4].ColumnWidth = 15;
            worksheet.Columns[5].ColumnWidth = 15;
            worksheet.Cells[1, dataGridView1.ColumnCount] = "КОЛ-ВО ЗАЯВОК";
            worksheet.Cells[1, 1] = "ФАМИЛИЯ ИМЯ ОТЧЕСТВО";
            worksheet.Cells[1, 2] = "ДОЛЖНОСТЬ";
            worksheet.Cells[1, 3] = "ЛОГИН";
            worksheet.Cells[1, 4] = "ПАРОЛЬ";


            ExcelLineType lineType = new ExcelLineType();
            lineType = ExcelLineType.Normal;
            for (int i = 1; i < dataGridView1.RowCount + 1; i++)
                for (int j = 1; j < dataGridView1.ColumnCount + 1; j++)
                    SetCellBorders(i, j, lineType);

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            { 
                for (int j = 0; j < dataGridView1.ColumnCount - 1; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = dataGridView1[j, i].Value.ToString();
                }
                worksheet.Cells[i + 2, dataGridView1.ColumnCount] = countTasksMassive[i];
            }     

            workBook.SaveAs(Path.Combine(Environment.CurrentDirectory, savedFileName));
            excel.Quit();
            

            closeProcess();
            Process process = new Process();
            process.Close();
        }

        private void closeProcess()
        {
            Process[] List;
            List = Process.GetProcessesByName("EXCEL");
            foreach (Process proc in List)
            {
                proc.Kill();
            }
        }

        public enum ExcelLineType { DotsSmall, Dots, None, Normal, Thick, Double }

        private void SetCellBorders(int rowNumber, int columnNumber, ExcelLineType lineType)
        {
            XlLineStyle lineStyle;
            XlBorderWeight lineWeight;
            setLineStyleAndWeight(lineType, out lineStyle, out lineWeight);

            Range cellRange = (Range)worksheet.Cells[rowNumber, columnNumber];
            Borders borders = cellRange.Borders;

            borders[XlBordersIndex.xlEdgeTop].LineStyle = lineStyle;
            borders[XlBordersIndex.xlEdgeTop].Weight = lineWeight;

            borders[XlBordersIndex.xlEdgeBottom].LineStyle = lineStyle;
            borders[XlBordersIndex.xlEdgeBottom].Weight = lineWeight;

            borders[XlBordersIndex.xlEdgeLeft].LineStyle = lineStyle;
            borders[XlBordersIndex.xlEdgeLeft].Weight = lineWeight;

            borders[XlBordersIndex.xlEdgeRight].LineStyle = lineStyle;
            borders[XlBordersIndex.xlEdgeRight].Weight = lineWeight;
        }

        private void setLineStyleAndWeight(ExcelLineType lineType, out XlLineStyle lineStyle, out XlBorderWeight lineWeight)
        {
            lineStyle = XlLineStyle.xlContinuous;
            lineWeight = XlBorderWeight.xlThin;

            if (lineType == ExcelLineType.DotsSmall)
            {
                lineStyle = XlLineStyle.xlDot;
                lineWeight = XlBorderWeight.xlHairline;
            }
            else if (lineType == ExcelLineType.Thick)
            {
                lineStyle = XlLineStyle.xlDot;
                lineWeight = XlBorderWeight.xlMedium;
            }
            else if (lineType == ExcelLineType.None)
            {
                lineStyle = XlLineStyle.xlLineStyleNone;
            }
            else if (lineType == ExcelLineType.Double)
            {
                lineStyle = XlLineStyle.xlDouble;
                lineWeight = XlBorderWeight.xlThick;
            }
            else if (lineType == ExcelLineType.Dots)
            {
                lineStyle = XlLineStyle.xlDot;
            }
        }

        private void countTasksFromUser(ref int[] countTasksMassive)
        {
            string SQLcommandUsers = "SELECT [id] FROM [Users]";
            dataTableUsers.Clear();
            dataCommand.CommandText = SQLcommandUsers;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTableUsers);
            int a = 0;

            


            foreach (DataRow rowUsers in dataTableUsers.Rows)
            {
                string SQLcommandTasks = "SELECT [user_id] FROM [Tasks] WHERE user_id = " + rowUsers[0].ToString();
                dataTableCountTasks.Clear();
                dataCommand.CommandText = SQLcommandTasks;
                dataAdapter.SelectCommand = dataCommand;
                dataAdapter.Fill(dataTableCountTasks);

                foreach (DataRow rowTasks in dataTableCountTasks.Rows)
                {
                    countTasks++;                
                }
                try
                {
                    //MessageBox.Show(countTasks.ToString());
                    countTasksMassive[a] = countTasks;
                    a++;
                    countTasks = 0;
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message + " 12321313");
                }
                
            }
        }

    }
}
