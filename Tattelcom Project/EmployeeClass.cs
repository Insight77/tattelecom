﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.ComponentModel;

namespace Tattelcom_Project
{
    public partial class EmployeeClass : Form
    {/*
        //_______________________________Запрещение перемещение формы__________________________________//

        const int SC_CLOSE = 0xF010;
        const int MF_BYCOMMAND = 0;
        const int WM_NCLBUTTONDOWN = 0x00A1;
        const int WM_NCHITTEST = 0x0084;
        
        const int HTCAPTION = 2;
        [DllImport("User32.dll")]
        static extern int SendMessage(IntPtr hWnd,
        int Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("User32.dll")]
        static extern bool RemoveMenu(IntPtr hMenu, int uPosition, int uFlags);

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_NCLBUTTONDOWN)
            {
                int result = SendMessage(m.HWnd, WM_NCHITTEST,
                IntPtr.Zero, m.LParam);
                if (result == HTCAPTION)
                    return;
            }
            base.WndProc(ref m);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            IntPtr hMenu = GetSystemMenu(Handle, false);
            RemoveMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
        }

        //___________________________________________________________________________________________//
        */

        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
        private string selectCommand = "SELECT id, fullname, Jobs.job_name, login, password, admin, Users.job_id FROM Users, Jobs WHERE Users.job_id=Jobs.job_id";

        private bool editMode = false; 
        private List<int> undoMode = new List<int>(100); // шаги действий
        private List<int> undoMode_idRow = new List<int>(100); // ids строк изменений
        private string[,] undoMode_saveInfo = new string[100, 20]; // сохранение первичной информации
        private int count_undoMode_saveInfo = -1; // своего рода id для первичной информации
        private int lastCreate_id = -1; // last id create
        private bool validUndo = true;
        
        private bool fastCancel = false; // быстрая отмена
        private int fastCancel_id = -1; // вид быстрой отмены

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        DataTable dataTable = new DataTable();

        private ToolStripMenuItem отменитьToolStripMenuItem;
        private DataGridView dataGridView1;
        private MenuStrip employee_menuStrip1;
        private ToolStripMenuItem создатьToolStripMenuItem;
        private TextBox fullName_textBox1;
        private TextBox login_textBox1;
        private TextBox pass_textBox1;
        private CheckBox admin_checkBox1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private ComboBox job_id_comboBox1;
        private ToolStripMenuItem удалитьToolStripMenuItem;
        private Panel panelCreate;
        private TextBox txtSearch;
        private Button btnSearch;
        private Button btnSave;

        

        public EmployeeClass()
        {
            InitializeComponent();
            dataBase.Open();
            loadDataGrid(selectCommand);
            lastCreate_id = dataGridView1.RowCount;
            dataGridViewConfiguration();
            panelCreate.Enabled = false;

            undoMode.Add(-1); // Начало
            undoMode_idRow.Add(-1); // Nachalo
            отменитьToolStripMenuItem.Enabled = false;
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.employee_menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.создатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fullName_textBox1 = new System.Windows.Forms.TextBox();
            this.login_textBox1 = new System.Windows.Forms.TextBox();
            this.pass_textBox1 = new System.Windows.Forms.TextBox();
            this.admin_checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.job_id_comboBox1 = new System.Windows.Forms.ComboBox();
            this.panelCreate = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.employee_menuStrip1.SuspendLayout();
            this.panelCreate.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(570, 315);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // employee_menuStrip1
            // 
            this.employee_menuStrip1.AllowMerge = false;
            this.employee_menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьToolStripMenuItem,
            this.удалитьToolStripMenuItem,
            this.отменитьToolStripMenuItem});
            this.employee_menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.employee_menuStrip1.Name = "employee_menuStrip1";
            this.employee_menuStrip1.Size = new System.Drawing.Size(593, 24);
            this.employee_menuStrip1.TabIndex = 1;
            this.employee_menuStrip1.Text = "employee_menuStrip";
            // 
            // создатьToolStripMenuItem
            // 
            this.создатьToolStripMenuItem.Name = "создатьToolStripMenuItem";
            this.создатьToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.создатьToolStripMenuItem.Text = "Создать";
            this.создатьToolStripMenuItem.Click += new System.EventHandler(this.создатьToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // отменитьToolStripMenuItem
            // 
            this.отменитьToolStripMenuItem.Name = "отменитьToolStripMenuItem";
            this.отменитьToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.отменитьToolStripMenuItem.Text = "Отменить";
            this.отменитьToolStripMenuItem.Click += new System.EventHandler(this.отменитьToolStripMenuItem_Click);
            // 
            // fullName_textBox1
            // 
            this.fullName_textBox1.Location = new System.Drawing.Point(4, 24);
            this.fullName_textBox1.Name = "fullName_textBox1";
            this.fullName_textBox1.Size = new System.Drawing.Size(159, 20);
            this.fullName_textBox1.TabIndex = 2;
            // 
            // login_textBox1
            // 
            this.login_textBox1.Location = new System.Drawing.Point(294, 24);
            this.login_textBox1.Name = "login_textBox1";
            this.login_textBox1.Size = new System.Drawing.Size(100, 20);
            this.login_textBox1.TabIndex = 4;
            // 
            // pass_textBox1
            // 
            this.pass_textBox1.Location = new System.Drawing.Point(400, 24);
            this.pass_textBox1.Name = "pass_textBox1";
            this.pass_textBox1.Size = new System.Drawing.Size(89, 20);
            this.pass_textBox1.TabIndex = 5;
            // 
            // admin_checkBox1
            // 
            this.admin_checkBox1.AutoSize = true;
            this.admin_checkBox1.Location = new System.Drawing.Point(4, 50);
            this.admin_checkBox1.Name = "admin_checkBox1";
            this.admin_checkBox1.Size = new System.Drawing.Size(146, 17);
            this.admin_checkBox1.TabIndex = 6;
            this.admin_checkBox1.Text = "Права Администратора";
            this.admin_checkBox1.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(494, 22);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "ФИО";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Должность";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(291, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Логин";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(397, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Пароль";
            // 
            // job_id_comboBox1
            // 
            this.job_id_comboBox1.FormattingEnabled = true;
            this.job_id_comboBox1.Items.AddRange(new object[] {
            "Сотрудник",
            "Менеджер",
            "Начальник"});
            this.job_id_comboBox1.Location = new System.Drawing.Point(167, 23);
            this.job_id_comboBox1.Name = "job_id_comboBox1";
            this.job_id_comboBox1.Size = new System.Drawing.Size(121, 21);
            this.job_id_comboBox1.TabIndex = 12;
            // 
            // panelCreate
            // 
            this.panelCreate.Controls.Add(this.login_textBox1);
            this.panelCreate.Controls.Add(this.fullName_textBox1);
            this.panelCreate.Controls.Add(this.job_id_comboBox1);
            this.panelCreate.Controls.Add(this.pass_textBox1);
            this.panelCreate.Controls.Add(this.label4);
            this.panelCreate.Controls.Add(this.admin_checkBox1);
            this.panelCreate.Controls.Add(this.label3);
            this.panelCreate.Controls.Add(this.btnSave);
            this.panelCreate.Controls.Add(this.label2);
            this.panelCreate.Controls.Add(this.label1);
            this.panelCreate.Location = new System.Drawing.Point(12, 348);
            this.panelCreate.Name = "panelCreate";
            this.panelCreate.Size = new System.Drawing.Size(599, 76);
            this.panelCreate.TabIndex = 14;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(279, 5);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(222, 20);
            this.txtSearch.TabIndex = 15;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(507, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 16;
            this.btnSearch.Text = "Найти";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // EmployeeClass
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(593, 432);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.panelCreate);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.employee_menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmployeeClass";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сотрудники";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EmployeeClass_FormClosed);
            this.Load += new System.EventHandler(this.EmployeeClass_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EmployeeClass_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.EmployeeClass_MouseMove);
            this.Move += new System.EventHandler(this.EmployeeClass_Move);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.employee_menuStrip1.ResumeLayout(false);
            this.employee_menuStrip1.PerformLayout();
            this.panelCreate.ResumeLayout(false);
            this.panelCreate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        // Настройки dataGridView1
        private void dataGridViewConfiguration()
        {
            dataGridView1.Columns[0].Visible = false; // id
            dataGridView1.Columns[1].Width = 200; // FIO
            dataGridView1.Columns[2].Width = 120; // job_name
            dataGridView1.Columns[3].Width = 90; // login
            dataGridView1.Columns[4].Width = 90; // pass
            dataGridView1.Columns[5].Width = 40; // admin    
            dataGridView1.Columns[6].Visible = false; // job_id
            dataGridView1.RowHeadersVisible = false;

            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);


            dataGridView1.ReadOnly = true;

            // comboBoxConfig 
            job_id_comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        // Загрузка данных в dataGridView1
        private void loadDataGrid(string SQLcommand)
        {
            dataTable.Clear();
            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
        }

        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clearInputs();
            panelCreate.Enabled = true;
            dataGridView1.Enabled = false;
            editMode = false;

            отменитьToolStripMenuItem.Enabled = true;
            fastCancel = true;
            fastCancel_id = 0;
        }

        // Событие при клике на ячейки dataGridView1
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int selectRow = dataGridView1.CurrentRow.Index; // Выделенная строка

            if (selectRow != dataGridView1.Rows[dataGridView1.RowCount - 1].Index)
            {
                fullName_textBox1.Text = dataGridView1[1, selectRow].Value.ToString();

                try
                {
                    int job_id = (int)dataGridView1[6, selectRow].Value;
                    job_id_comboBox1.SelectedIndex = --job_id;
                }
                catch { }

                login_textBox1.Text = dataGridView1[3, selectRow].Value.ToString();
                pass_textBox1.Text = dataGridView1[4, selectRow].Value.ToString();

                if (dataGridView1[5, selectRow].Value.ToString() == "1")
                    admin_checkBox1.Checked = true;
                else
                    admin_checkBox1.Checked = false;

                panelCreate.Enabled = true;
                editMode = true;
            }
            else
            {
                clearInputs();
                panelCreate.Enabled = false;
                editMode = false;
            }
            
        }

        // Очистка полей
        private void clearInputs()
        {
            try
            {
                dataGridView1.CurrentRow.Selected = false;
            } catch { }
            fullName_textBox1.Clear();
            job_id_comboBox1.SelectedIndex = 0;
            login_textBox1.Clear();
            pass_textBox1.Clear();
            admin_checkBox1.Checked = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string SQLQuery;
            int job_id = job_id_comboBox1.SelectedIndex;

            if (validInfo())
            {
                if (editMode)
                {
                    int selectedColumn = dataGridView1.CurrentRow.Index;
                    SQLQuery = string.Format("UPDATE [Users] SET [fullname]='{0}', [job_id]='{1}', [login]='{2}', [password]='{3}', [admin]='{4}' WHERE [id]={5}",
                        fullName_textBox1.Text,
                        ++job_id,
                        login_textBox1.Text,
                        pass_textBox1.Text,
                        (admin_checkBox1.Checked ? "1" : "0"),
                        dataGridView1[0, selectedColumn].Value.ToString()
                    );

                    undoMode.Add(0); // Добавление шага редактирования
                    undoMode_idRow.Add((int)dataGridView1[0, selectedColumn].Value); // Добавление id строки редактирования

                    saveInfo(selectedColumn);
                }
                else
                {
                    SQLQuery = string.Format("INSERT INTO [Users] ([fullname], [job_id], [login], [password], [admin], [id]) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', {5})",
                        fullName_textBox1.Text,
                        ++job_id,
                        login_textBox1.Text,
                        pass_textBox1.Text,
                        (admin_checkBox1.Checked ? "1" : "0"),
                        ++lastCreate_id
                    );



                    undoMode.Add(1); // Добавление шага сохранения новой записи
                    undoMode_idRow.Add(lastCreate_id); // Добавление id строки изменения                               
                }

                try
                {
                    UpdateDataBase(SQLQuery);
                    MessageBox.Show("Запись сохранена", " успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                panelCreate.Enabled = false;
                отменитьToolStripMenuItem.Enabled = true;
                fastCancel = false;
                clearInputs();
                dataGridView1.Enabled = true;
            }
                     
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedColumn = dataGridView1.CurrentRow.Index; // Выделенная строка
                saveInfo(selectedColumn);
                undoMode_idRow.Add((int)dataGridView1[0, selectedColumn].Value);

                string SQLQuery = string.Format("DELETE FROM [Users] WHERE [id]={0}",
                    dataGridView1[0, selectedColumn].Value.ToString()
                );

                try
                {
                    UpdateDataBase(SQLQuery);
                    MessageBox.Show("Запись удалена", " успешно ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                clearInputs();
                dataGridView1.Enabled = true;
                panelCreate.Enabled = false;

                отменитьToolStripMenuItem.Enabled = true;
                fastCancel = false;
                undoMode.Add(2); // Добавление шага удаления
                                 // MessageBox.Show(undoMode_idRow.Last().ToString());
            }
            catch { }
        }

        private void EmployeeClass_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            
            //поисковая строка 
            string word = txtSearch.Text;

            //строка запроса с фильтром 
            string queryString = "SELECT id, fullname, Jobs.job_name, login, password, admin, Users.job_id FROM Users, Jobs WHERE Users.job_id=Jobs.job_id" + 
                                 " AND (Users.fullname LIKE '%" + word + "%'" +
                                 " OR Jobs.job_name LIKE '%" + word + "%'" +
                                 " OR Users.login LIKE '%" + word + "%'" +
                                 " OR Users.password LIKE '%" + word + "%')";

            //вызов метода для заполнения грида с данными найденными по  фильтру 
            clearInputs();
            loadDataGrid(queryString);

            отменитьToolStripMenuItem.Enabled = true;
            fastCancel = true;
            fastCancel_id = 1;
        }

        private void EmployeeClass_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void EmployeeClass_Move(object sender, EventArgs e)
        {
            
        }

        private void отменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fastCancel)
            {
                // Отменить создание записи
                if(fastCancel_id == 0)
                {
                    clearInputs();
                    panelCreate.Enabled = false;
                    dataGridView1.Enabled = true;              
                }

                if(fastCancel_id == 1)
                {
                    loadDataGrid(selectCommand);
                }

                fastCancel = false;
            }
            else
            {
                // Отменить редактирование записи
                if ((undoMode.Last() == 0) && validUndo)
                {                    
                    int lastRow = count_undoMode_saveInfo;
                    
                    string SQLQuery = string.Format("UPDATE [Users] SET [fullname]='{0}', [job_id]='{1}', [login]='{2}', [password]='{3}', [admin]='{4}' WHERE [id]={5}",
                        undoMode_saveInfo[lastRow, 0],
                        Convert.ToInt32(undoMode_saveInfo[lastRow, 1]),
                        undoMode_saveInfo[lastRow, 2],
                        undoMode_saveInfo[lastRow, 3],
                        undoMode_saveInfo[lastRow, 4],
                        undoMode_idRow.Last().ToString()
                    );

                    UpdateDataBase(SQLQuery);

                    undoMode.RemoveAt(undoMode.Count - 1);
                    undoMode_idRow.RemoveAt(undoMode_idRow.Count - 1);
                    count_undoMode_saveInfo--;
                    validUndo = false;
                }

                // Отменить сохранение новой записи
                if ((undoMode.Last() == 1) && validUndo)
                {
                    string SQLQuery = string.Format("DELETE FROM [Users] WHERE [id]={0}",
                        undoMode_idRow.Last().ToString()
                    );

                    UpdateDataBase(SQLQuery);

                    undoMode.RemoveAt(undoMode.Count - 1);
                    undoMode_idRow.RemoveAt(undoMode_idRow.Count - 1);
                    validUndo = false;
                }
                
                // Отменить удаление записи
                if ((undoMode.Last() == 2) && validUndo)
                {
                    int lastRow = count_undoMode_saveInfo;
                    // MessageBox.Show((undoMode_idRow.Last()).ToString());
                    string SQLQuery = string.Format("INSERT INTO [Users] ([fullname], [job_id], [login], [password], [admin], [id]) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', {5})",
                    undoMode_saveInfo[lastRow, 0],
                    Convert.ToInt32(undoMode_saveInfo[lastRow, 1]),
                    undoMode_saveInfo[lastRow, 2],
                    undoMode_saveInfo[lastRow, 3],
                    undoMode_saveInfo[lastRow, 4],
                    undoMode_idRow.Last()
                );

                    UpdateDataBase(SQLQuery);

                    undoMode.RemoveAt(undoMode.Count - 1);
                    undoMode_idRow.RemoveAt(undoMode_idRow.Count - 1);
                    count_undoMode_saveInfo--;
                    validUndo = false;
                }
            }

            if (undoMode.Last() == -1) отменитьToolStripMenuItem.Enabled = false;
            validUndo = true;
        }

        private void UpdateDataBase(string SQLQuery)
        {
            try
            {
                dataCommand.CommandText = SQLQuery;
                dataCommand.Connection = dataBase;
                dataAdapter.SelectCommand = dataCommand;
                dataCommand.ExecuteNonQuery(); // Проверка на изменение строк

                loadDataGrid(selectCommand);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void saveInfo(int selectedColumn)
        {
            count_undoMode_saveInfo++;
            undoMode_saveInfo[count_undoMode_saveInfo, 0] = dataGridView1[1, selectedColumn].Value.ToString();
            if (dataGridView1[2, selectedColumn].Value.ToString() == "Сотрудник") undoMode_saveInfo[count_undoMode_saveInfo, 1] = "1";
            else if (dataGridView1[2, selectedColumn].Value.ToString() == "Менеджер") undoMode_saveInfo[count_undoMode_saveInfo, 1] = "2";
            else undoMode_saveInfo[count_undoMode_saveInfo, 1] = "3";
            undoMode_saveInfo[count_undoMode_saveInfo, 2] = dataGridView1[3, selectedColumn].Value.ToString();
            undoMode_saveInfo[count_undoMode_saveInfo, 3] = dataGridView1[4, selectedColumn].Value.ToString();
            undoMode_saveInfo[count_undoMode_saveInfo, 4] = dataGridView1[5, selectedColumn].Value.ToString();
        }

        private bool validInfo()
        {
            bool clearBox = false;
            bool matches = false;

            if (fullName_textBox1.Text == "") clearBox = true;
            if (login_textBox1.Text == "") clearBox = true;
            if (pass_textBox1.Text == "") clearBox = true;

            if (clearBox)
            {
                MessageBox.Show("Одно из полей является пустым!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (editMode)
            {
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                    if (fullName_textBox1.Text == dataGridView1[1, i].Value.ToString())
                    {
                        if (job_id_comboBox1.SelectedItem.ToString() == dataGridView1[2, i].Value.ToString())
                        {
                            if (login_textBox1.Text == dataGridView1[3, i].Value.ToString())
                            {
                                if (pass_textBox1.Text == dataGridView1[4, i].Value.ToString())
                                {
                                    if ((admin_checkBox1.Checked ? "1" : "0") == dataGridView1[5, i].Value.ToString())
                                    {
                                        MessageBox.Show("Такой сотрудник уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        matches = true;
                                    }
                                }
                            }
                        }
                    }

                /*for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                    if (login_textBox1.Text == dataGridView1[3, i].Value.ToString())
                    {
                        if (job_id_comboBox1.SelectedItem.ToString() == dataGridView1[2, i].Value.ToString())
                        {
                            if (login_textBox1.Text == dataGridView1[1, i].Value.ToString())
                            {
                                MessageBox.Show("Такой логин уже зарегистрирован!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                matches = true;
                            }
                        }

                    }*/
            }
            else
            {
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                    if (fullName_textBox1.Text == dataGridView1[1, i].Value.ToString())
                    {
                        MessageBox.Show("Такой сотрудник уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                    if (login_textBox1.Text == dataGridView1[3, i].Value.ToString())
                    {
                        MessageBox.Show("Такой логин уже зарегистрирован!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
            }
            
            if (matches) return false;

            return true;
        }

        private void EmployeeClass_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataBase.Close();
            this.Dispose();   
        }

        private void EmployeeClass_Load(object sender, EventArgs e)
        {

        }
    }
}
