﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattelcom_Project
{
    class Gistogramma : Form
    {
        private static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
        private string selectCommand = "SELECT date_begin_hour FROM Tasks";

        OleDbConnection dataBase = new OleDbConnection(connectionString);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        OleDbCommand dataCommand = new OleDbCommand();
        DataTable dataTable = new DataTable();

        int min = 0, max = 1;
        int[] hours;
        int[] numberOfClients = new int[24];
        private Label label1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;

        public Gistogramma()
        {
            InitializeComponent();
            loadData(dataTable, selectCommand);
            hours = new int[dataTable.Rows.Count];
            loadChart();
        }

        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(12, 36);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Clients";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(716, 418);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Количество клиентов с ";
            // 
            // Gistogramma
            // 
            this.ClientSize = new System.Drawing.Size(740, 466);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chart1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Gistogramma";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void loadData(DataTable dataTable, string SQLcommand)
        {
            dataCommand.CommandText = SQLcommand;
            dataCommand.Connection = dataBase;
            dataAdapter.SelectCommand = dataCommand;
            dataAdapter.Fill(dataTable);
        }

        private void loadChart()
        {
            FindDataForChart();
            for (int i = min; i < max + 1; i++)
            {
                chart1.Series["Clients"].Points.AddXY(i, numberOfClients[i]);
            }            
        }

        private void FindDataForChart()
        {
            int a = 0, matches = 0;
            bool b = true;

            foreach (DataRow row in dataTable.Rows)
            {
                hours[a] = Convert.ToInt32(row[0]);
                a++;
            }

            min = hours[0];
            max = hours[hours.Length - 1];
            Array.Sort(hours);
            for(int i = 0; i < hours.Length; i++)
            {
                if (min > hours[i])
                    min = hours[i];
                if (max < hours[i])
                    max = hours[i];
            }

            label1.Text = "Количество клиентов с " + min + " по " + max + " часов.";

            for(int i = 0; i < 24; i++)
            {
                for(int j = 0; j < hours.Length; j++)
                {
                    if(i == hours[j])
                    {
                        matches++;
                    }
                }
                numberOfClients[i] = matches;
                matches = 0;
            }
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
