﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;


namespace Tattelcom_Project
{
    public partial class AuthClass : Form
    {
        bool validEnter = false;
        private TextBox textBoxLogin;
        private TextBox textBoxPassword;
        private Button btn_Register;
        private DataGridView dataGridView1;
        private Label label1;
        private Label label2;
        public OleDbConnection database;  // для подключения базы данных 
        public string personName = "";
        public int admin = -1;
     
        public AuthClass()
        {
            InitializeComponent();
        }

        public void loadDataGrid(string sqlQueryString)
        {

            OleDbCommand SQLQuery = new OleDbCommand();
            DataTable data = null;
            dataGridView1.DataSource = null;
            SQLQuery.Connection = null;
            OleDbDataAdapter dataAdapter = null;
            dataGridView1.Columns.Clear();

            SQLQuery.CommandText = sqlQueryString;
            SQLQuery.Connection = database;
            data = new DataTable();
            dataAdapter = new OleDbDataAdapter(SQLQuery);
            dataAdapter.Fill(data);
            dataGridView1.DataSource = data;

            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;          // <-- dataGridView1 только для чтения 

        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthClass));
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.btn_Register = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(47, 7);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(102, 20);
            this.textBoxLogin.TabIndex = 3;
            this.textBoxLogin.Tag = "";
            this.textBoxLogin.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(47, 36);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(102, 20);
            this.textBoxPassword.TabIndex = 4;
            this.textBoxPassword.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.textBoxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxPassword_KeyDown);
            // 
            // btn_Register
            // 
            this.btn_Register.Location = new System.Drawing.Point(12, 62);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.Size = new System.Drawing.Size(129, 23);
            this.btn_Register.TabIndex = 6;
            this.btn_Register.Text = "Продолжить";
            this.btn_Register.UseVisualStyleBackColor = true;
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 88);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(100, 85);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Логин";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Пароль";
            // 
            // AuthClass
            // 
            this.ClientSize = new System.Drawing.Size(153, 90);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_Register);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthClass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторизация";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.RegisterClass_FormClosed);
            this.Load += new System.EventHandler(this.RegisterClass_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void auth()
        {
            //соединение с базой access mydb.accdb
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|mydb.accdb";
            string login = textBoxLogin.Text.ToString().Trim();
            string password = textBoxPassword.Text.ToString().Trim();
            //.ToString();
            try
            {                
                    database = new OleDbConnection(connectionString);
                    database.Open();
                    //SQL query для выборки данных queryString из таблицы сотрудников person и таблицы job 
                    string queryString = "SELECT fullname, login, password, admin FROM Users WHERE login='" + login + "'"; //FROM klient where fam LIKE'" + nn + "%'";

                    //вызов функции  loadDataGrid для заполнения объекта grid данными из выборки  queryString  
                    loadDataGrid(queryString);

                    admin = (int)dataGridView1[3, 0].Value;
                    personName = dataGridView1[0, 0].Value.ToString();

                    if (password == dataGridView1[2, 0].Value.ToString())
                    {
                        MessageBox.Show(" Сотрудник " + personName + ", доступ к данным разрешен! ");
                        validEnter = true;               
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Пожалуйста проверьте ввод данных. Вход прошел не удачно! ");                   
                    }
                
            }
            catch (Exception ex)
            {
                //сообщение об ошибке 
                MessageBox.Show("В базе данных нет ни одного сотрудника!");
                return;
            }
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
            auth();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void RegisterClass_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void RegisterClass_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (validEnter == false) Application.Exit();
        }

        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) auth();
        }

        public string Test()
        {
            string test ="";
            return test;
        }
    }

}

